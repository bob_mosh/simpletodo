//
//  SimpleToDoApp.swift
//  SimpleToDo
//
//  Created by Ferdinand Goeldner on 15.09.20.
//

import SwiftUI

@main
struct SimpleToDoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
