//
//  ContentView.swift
//  SimpleToDo
//
//  Created by Ferdinand Goeldner on 15.09.20.
//

import SwiftUI

struct ContentView: View {
    // Data Sources
    @State var newItemText = ""
    @State var tasks: [TaskItem] = [TaskItem(name: "Präsentation vorbereiten.", finished: true),
                                    TaskItem(name: "Präsentation halten.", finished: false),
                                    TaskItem(name: "Workshop halten.", finished: false),
                                    TaskItem(name: "Feiern. 🎉", finished: false)]

    var body: some View {
        // Layout
        NavigationView {
            Form {
                // User Input
                TextField("New Item", text: $newItemText, onCommit:  {
                    tasks.append(TaskItem(name: newItemText, finished: false))
                    newItemText = ""
                })

                // Data Display
                Section(header: Text("Items")) {
                    ForEach(tasks, id: \.self) { (task) in
                        ItemView(item: task)
                    }.onDelete(perform: { indexSet in
                        guard indexSet.first != nil else { return }
                        tasks.remove(at: indexSet.first!)
                    })
                }
            }.navigationTitle("Simple ToDo")
        }
    }
}

struct ItemView: View {
    // Data Sources
    @State var item: TaskItem

    var body: some View {
        HStack {
            Image(systemName: item.finished ? "checkmark.circle.fill" : "checkmark.circle")
                .foregroundColor(.accentColor)
            Text(item.name)
                .opacity(item.finished ? 0.3 : 1.0)
        }.onTapGesture {
            item.finished = !item.finished
        }
    }
}

struct TaskItem: Hashable {
    var name: String
    var finished: Bool
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
