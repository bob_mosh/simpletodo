//
//  ContentView.swift
//  SimpleToDo
//
//  Created by Ferdinand Goeldner on 18.09.20.
//

import SwiftUI

struct ContentView: View {
    var newItemText = ""
    var tasks: [TaskItem] = [TaskItem(name: "Präsentation vorbereiten.", finished: true),
                             TaskItem(name: "Präsentation halten.", finished: false),
                             TaskItem(name: "Workshop halten.", finished: false),
                             TaskItem(name: "Feiern. 🎉", finished: false)]

    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct TaskItem: Hashable {
    var name: String
    var finished: Bool
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
